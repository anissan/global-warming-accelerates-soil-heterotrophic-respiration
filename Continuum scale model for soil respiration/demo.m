clc
clear all
close all
%% static variables
gamma1      = 100;                          % Scaling coeffiecant for water cluster
tau         = 0.218;                        % Scaling coeffiecant for water cluster
Cdoc        = 1.95;                         % Dissolved organic carbon concentration [mol/m^3]
L           = 0.1;                          % Characteristic length size of the porous media [m]
ell         = 3e-04;                        % Characteristic grain size [m] 
Es          = 9e4;                          % activation energy for Michaelis–Menten; see Davidson et al,2011  https://doi.org/10.1111/j.1365-2486.2011.02546.x
as          = 1e11;                         % pre-exponential for Michaelis–Menten; see Davidson et al,2011  https://doi.org/10.1111/j.1365-2486.2011.02546.x  
h           = 0;                            % Altitude [m]
por         = 0.4;                          % Porosity

                                        
%% dynamic variables
Sat         = linspace(0.01,0.99,30);     % Soil relative saturation  
T           = linspace(273,310,30);       % Soil temperature [K]


%% Running
surfHR = nan(length(T),length(Sat));
for i = 1:length(Sat)
    for j = 1:length(T)
       
        Tk          = T(j) ; 
        sat_n       = Sat(i);
        xi          = gamma1.*(1-sat_n);
        NP          = ((L/ell).^3).^(1-sat_n).*(((1./xi).^(tau-1)).*gamma(1-tau) - expint(1./xi));
        Vw          = (sat_n).*L.^3;
        SP          = (Vw./NP);
        R0          = ((3.*SP./1)./(pi.*4)).^(1./3);

        HR          =  odeGlobal(Cdoc,ell,R0,Tk,h,por,NP,as,Es);   % HR flux
        surfHR(j,i) = HR;

    end
end

%% Plotting
contourf(Sat,T,log10(surfHR))
