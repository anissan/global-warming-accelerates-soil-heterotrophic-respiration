function HR = odeGlobal(Cdoc,ell,R0,Tk,h,Por,NP,as_fac,Ea_s)
SSA         = 0.003044*((ell/2).^-0.5961);
SSAn        = 0.003044*(((0.3*1e-3)/2).^-0.5961);
a_s         = as_fac.*(SSAn).*(SSA./SSAn); % mol/(m^3·s)
kH          = 0.0013*exp(1500*((1/Tk) - 1/(298.15)));  % (mol/(kg*bar))
p0          = 101325; %Pa
M           = 0.02896; %kg/mol
R_c         = 8.3144; % [J/(K*mol)]
g           = 9.81;
PP          = (p0*exp((M*g*h)./(R_c*Tk))).*1e-5; % bar
C0          = (PP*kH)*1000; % mol/m^3
C0(C0> 2)   = 1.99;

Vmax        = a_s.*exp(-Ea_s./(R_c.*Tk));  % mol/(m^3*s)
Cs          = Cdoc;
kMo         = 0.05;
kMs         = 0.1;
Dm          = (10^(-4.41 +(773.8/Tk) - (506/Tk)^2))./(100*100);
Rc          = (3*Por*Vmax/ell)*(Cs/(Cs+kMs));




m = 2;
xmesh       = linspace(0,R0,500);
tspan       = linspace(1,1e5,10);
pde_an      = @(x,t,u,dudx) pdefun(x,t,u,dudx,Dm,Rc,kMo);
bc_an       = @(xl,ul,xr,ur,t) bcfun(xl,ul,xr,ur,t,C0);
sol         = pdepe(m,pde_an,@icfun,bc_an,xmesh,tspan);
sol_u       = sol(end,:); % taking the last solution, ~steady state
sol_reac    = ((3.*Vmax.*Por./ell)*(Cs./(Cs+kMs)) .* (sol_u./(sol_u+kMo)));% converting from oxygen concentation within the patch to reaction ---> based on Michaelis–Menten
Rv          = linspace(xmesh(1),xmesh(end),length(xmesh)).^3;
volR        = (4/3).*pi.* diff(Rv);
Hr_wp       = mean([sol_reac(1:end-1);sol_reac(2:end)]);
solSum      = sum(Hr_wp.*volR);
fac         = (60*60*24*365);  % convert from sec to year
HR          = solSum.*NP.*fac;






    function [c,f,s] = pdefun(x,t,u,dudx,Dm,Rc,kMo)
        c = 1/Dm;
        f = dudx;
        s = -(1/Dm)*(Rc)*(u/(u+kMo));
    end


    function u0 = icfun(x)
        u0 = 0;
    end


    function [pl,ql,pr,qr] = bcfun(xl,ul,xr,ur,t,C0)
        pl = 0;
        ql = 1;
        pr = ur-C0;
        qr = 0;
    end





end