function output =  check_overlapping(data_grains,x0,y0,r0,output,th,pN)

str = sprintf('overlapping....');
single_point = [x0,y0];
xV = data_grains(:,1);
yV = data_grains(:,2);
distances = sqrt(sum((data_grains(:,[1,2]) - single_point) .^ 2, 2)) - (r0+data_grains(:,3));


if min(distances) > th
    output = 'False';
    
else
    disp(str);
end




end