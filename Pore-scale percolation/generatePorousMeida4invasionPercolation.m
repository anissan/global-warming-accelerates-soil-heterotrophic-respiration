clc
clear all;
close all;

%%  Units in mm
Lx = 100;
Ly = 200;
gap = 4;



%% Distribution
% pick from uniorm, normal and lognoraml
distribution = 'lognormal';
muGrain = 1;
sigma = muGrain;
nameStep = strcat(distribution,'_',num2str(muGrain),'_',num2str(sigma),'.dxf');

minVal = muGrain/50;
maxVal = 5;
Porosity = 0.6;
Vt = Lx*Ly;
th = 1e-3;
realizations = 20;

for k =1:realizations
    data_grains = [];
    At = Lx*Ly;
    disArea = -(At*(Porosity-1));
    allR = [];
    allR =  randomCircles(disArea,muGrain,sigma,th,distribution,minVal,maxVal);
    pN = 1;
    
    nameNewDir = strcat('dis_',distribution,'_mean_',num2str(muGrain),'_std_',num2str(sigma),'_por_',num2str(Porosity));
    mkdir(nameNewDir)
    allR = fliplr(sort(allR));
    %%
    Vs = 0;
    Vp = Vt-Vs;
    real = 20;
    output = 'True';
    np = 1;
    
    while (Vp/Vt > Porosity)
        r0 = allR(np);
        
        while strcmp(output,'True') == 1
            [x0,y0] = new_cyl(minVal,maxVal,muGrain,sigma,Lx,Ly);
            if pN > 1
                output = check_overlapping(data_grains,x0,y0,r0,output,th,pN);
            else
                output = 'False';
            end
            
        end
        Vs = Vs +(pi.*(r0^2));
        Vp = Vt-Vs;
        a = Vp/Vt;
        data_grains = [data_grains;[x0,y0,r0]];
        output = 'True';
        pN = pN + 1;
        np = np + 1;
        circle(x0,y0,r0); hold on
        str = sprintf('Por = %f',a);
        disp(str);
    end
    
    
    
    close all
    histogram((data_grains(:,3)),200,'Normalization','probability')
    xlabel ('Radius [mm]')
    ylabel ('PDF')
    close all
    mu1 = mean(data_grains(:,3));
    std1 = std(data_grains(:,3));
    
    
    
    
    figure; hold on
    for i = 1 : size(data_grains,1)
        rectangle('Curvature', [1 1], ...
            'Position', [data_grains(i,1:2)-data_grains(i,3) repmat(2*data_grains(i,3),1,2)], ...
            'facecolor', 'k', 'edgecolor', 'none') %// plot filled circle
    end
    
    
    
    pbaspect([Lx/Lx Ly/Lx 1])
    set(gca,'YTickLabel',[]);
    set(gca,'XTickLabel',[]);
    set(gcf,'units','normalized','outerposition',[0 0 1 1])
    set(gca,'visible','off')
    print(gcf,'foo.png','-dpng','-r1200');
    close all
    
    im = imread('foo.png');
    imshow(im)
    if k == 1
        [xs,ys] = ginput(1);
        [xe,ye] = ginput(1);
    end
    
    im = imcrop(im,[xs,ys,xe-xs,(ye-ys)]);
    im = rgb2gray(im);
    rows = 2000 .* (Ly/Lx);
    col  = 2000;
    BW = imresize(im,[rows,col]);
    BW = im2bw(BW,0.5);
    nameFile = strcat('big_real_',num2str(k),'.mat');
    nameSave = strcat(nameNewDir,'/',nameFile);
    save(nameSave,'BW','Lx');
    close all
end
