#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 11 10:17:19 2023

@author: alonnissan
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 08:20:03 2021

@author: Nissan
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Oct 22 11:23:23 2021

@author: Nissan
"""

import porespy as ps
import numpy as np
import matplotlib.pyplot as plt
from edt import edt
import imageio
from copy import copy
import scipy.io
# from mat4py import loadmat
import glob
import os
from scipy.io import savemat





ps.visualization.set_mpl_style()
from joblib import Parallel, delayed


def pnmRun(i,mat_files):
        nameOpen = mat_files[i]
        mat = scipy.io.loadmat(nameOpen)
        mat = mat['BW']
    
        sizeX = np.size(mat,0)
        sizeY = np.size(mat,1)
        im = mat > 0        
        im_org = imbd = np.zeros_like(im, dtype=bool)
        bd = np.zeros_like(im, dtype=bool)
        bd[0, :] = 1
        bd *= im
        dt = edt(im)
        out = ps.filters.ibip(im=im, inlets=bd, maxiter=100000)
        inv_seq, inv_size = out.inv_sequence, out.inv_sizes
        inv_satn = ps.filters.seq_to_satn(seq=inv_seq)
        
        
        
        
        
        
        
        fig, ax = plt.subplots(1, 2, figsize=[8, 4])
        s = ax[0].imshow(inv_satn/im, origin='lower', interpolation='none')
        
        max_sat = np.amax(inv_satn)
        subDir = glob.glob('/Users/alonnissan/Desktop/Nissan_et_al_2023/Percolation/dis_lognormal_mean_1_std_1_por_0.6/')        
        print("Saturation = {}".format(1-max_sat))
        mdic = {"inv_satn": inv_satn/im}
        nameFile = nameDir + 'mat_matrix_' + str(np.round(1-max_sat, 2)) + '_' + nameOpen
        print("Saving....Saturation = {}".format(1-max_sat))
        savemat(nameFile, mdic)
        
        
        
     
        return print("Saving....Saturation = {}".format(1-max_sat))




ps.visualization.set_mpl_style()
subDir = glob.glob('/Users/alonnissan/Desktop/Nissan_et_al_2023/Percolation/dis_lognormal_mean_1_std_1_por_0.6/')

nameDir = subDir[0]
os.chdir(nameDir)
# Get a list for .mat files in current folder
mat_files = glob.glob('big*.mat') 
# List for stroring all the data
Parallel(n_jobs=-2)(delayed(pnmRun)(i,mat_files) for i in range(len(mat_files)))



