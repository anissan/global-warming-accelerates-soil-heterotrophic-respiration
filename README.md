# Global warming accelerates soil heterotrophic respiration 



## Getting started

This repository accommodates computer codes and data which associated with the manuscript _Global warming accelerates soil heterotrophic respiration_ submitted to _Nature Communications_.





## Description
Our study presents a continuum scale model designed to estimate soil heterotrophic respiration in different ecosystems. The model is based on two main components: percolation theory-based scaling laws and the Diffusion-Reaction equation. Percolation theory is used to account for water distribution in the soil matrix, while the Diffusion-Reaction equation describes heterotrophic respiration under steady-state conditions.
<br>
The integration of these two components results in a comprehensive and simple model capable of predicting soil heterotrophic respiration rates with high accuracy. By using this model, we can obtain a better understanding of how soil heterotrophic respiration varies across ecosystems, which can have implications for carbon cycling and climate change.
<br>
<br>
For a more detailed description of the model, we invite readers to consult our manuscript. 









## System requirements
**Tested on:**
<br>
Windows 10 64 bit
<br>
macOs Monterey

**Dependencies (tested version):**
<br>
Python 3.8.13 
> - numpy 1.22.4 <br>    
> - matplotlib 3.5.2 <br>  
>  - porespy 2.2.2 <br>  
>  - imageio 2.22.2 <br>
>  - scipy 1.8.1 <br>
<br>
MATLAB R2022a     


## Authors 
Authors: Alon Nissan, Uria Alcolombri, Nadav Peleg, Nir Galili, Joaquin Jimenez-Martinez, Peter Molnar and Markus Holzner
<br>
Correspondence to: Alon Nissan (anissan@ethz.ch)


## Demo
To execute the proposed continuum model for soil respiration outlined in the manuscript, please refer to the demo located in the "Continuum Scale Model for Soil Respiration" folder. By running the Matlab file named "demo.m", you can investigate the impact of various static variables (e.g., porosity, dissolved organic carbon, grain size) and dynamic arrays for soil temperature and moisture content on soil heterotrophic respiration.



